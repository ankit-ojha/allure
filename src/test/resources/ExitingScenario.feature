Feature: Gas Dispatching
  1) This feature is to creates the Shipper Nominations and gas scheduling.

  @regression @ECPT-138 @transport
  Scenario: This test create the input_output nominations for gas scheduling through calculations.
    Given I login to EC with valid credentials
    When I navigate to "Daily Input Nomination" screen and enter navigation data
      | Date       | Business Unit | Contract Area | Contract  | Nomination Point              |
      | 2018-03-18 | TS3 BU1       | TS3_FIRM      | TS3_FIRM1 | ENTRY-TS3_FIRM1-TS3_STAVANGER |
    And I create daily nominations on "Daily Input Nomination" screen
      | Contract  | Nomination Point              | Requested Qty |
      | TS3_FIRM1 | ENTRY-TS3_FIRM1-TS3_STAVANGER | 25,000        |
    And I navigate to "Daily Output Nomination" screen and enter navigation data
      | Date       | Business Unit | Contract Area | Contract  | Nomination Point              |
      | 2018-03-18 | TS3 BU1       | TS3_FIRM      | TS3_FIRM1 | ENTRY-TS3_FIRM1-TS3_STAVANGER |
    And I create daily nominations on "Daily Output Nomination" screen
      | Contract  | Nomination Point              | Requested Qty |
      | TS3_FIRM1 | ENTRY-TS3_FIRM1-TS3_STAVANGER | 25,000        |
    And I navigate to "Daily Nomination Balancing" screen and enter navigation data
      | Date       | Business Unit | Contract Area | Contract  |
      | 2018-03-18 | TS3 BU1       | TS3_FIRM      | TS3_FIRM1 |
    And I submit created input and output nomination
    And I navigate to "System Attributes" screen
    Then I verify attribute present on the screen
      | Date       | Atrribute Type | Attribute Value | Description               |
      | 2024-01-01 | UTC2LOCAL_DIFF | 1               | Change to summertime 2024 |
    When I navigate to "Create Calculation" screen and enter navigation data
      | Date       | Calculation Context  |
      | 2011-01-01 | Transport Allocation |
    And I remove existing calculation jobs on the screen
      | Calculation Job            |
      | TS3 Disp Adjustment Calc   |
      | TS3 Disp Confirmation Calc |
      | TS3 Disp Schedule Calc     |
    And I execute the sql files "to create calculation jobs"
      | SQL Files                          |
      | calculation_TS3_DISP_CALC_ADJ.sql  |
      | calculation_TS3_DISP_CALC_CONF.sql |
    And I navigate to "Alloc Network Calc Job Conn" screen and enter navigation data
      | Calculation Group |
      | TS3 Alloc Net     |
    And I add inserted calculation jobs to Alloc Network Calc Job Conn screen
      | Date       | Calculation Job          |
      | 2011-01-01 | TS3 Disp Adjustment Calc |
    And I navigate to "Daily Dispatching Calculation" screen under "EC Transport" module and "Scheduling" submodule and enter navigation data
      | Calculation Network | Calculation Job          |
      | TS3 Alloc Net       | TS3 Disp Adjustment Calc |
    Then "TS3 Disp Adjustment Calc" Calculation should run successfully
    And I logout from application

  @smoke @transport
  Scenario: Smoke and Transport Tag
    Given I login to EC with valid credentials

  @smoke
  Scenario: Smoke Tag only
    Given I login to EC with valid credentials

  @transport
  Scenario: Transport Tag only
    Given I login to EC with valid credentials

  @regression
  Scenario: Regression Tag only
    Given I login to EC with valid credentials

  @smoke @transport @wip
  Scenario: Regression Tag only
    Given I login to EC with valid credentials